target := server
flags := -d:danger -d:release

all: $(target)

$(target): $(target).nim
	nim c $(flags) -o:$@ $<

clean:
	rm -fv $(target)
