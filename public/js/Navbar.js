function NavBar() {
    var button = document.querySelector('#button');
    var blob = document.querySelector('#inners');
    var links = document.querySelectorAll('.inInner');

    links.forEach(link => {
        link.classList.toggle('inInner');
    });
    button.addEventListener("click", function () {
        blob.classList.toggle('hidden');
        links.forEach((link, index) => {
            setTimeout(() => {
                    link.classList.toggle('hidden');
                    link.classList.toggle('inner');
                },
                index *
                15);
        })
    })
}
NavBar();