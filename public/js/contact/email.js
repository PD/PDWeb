function email() {
    const el = document.createElement('textarea');
    el.value = 'support@pden.net';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    Swal.fire({
        icon: 'success',
        title: 'Your mail client should have opened. If not, manually enter support@pden.net in the Send field',
        background: '#1d1e22',
        padding: '6rem',
        showConfirmButton: false,
        timer: false
    })
}
