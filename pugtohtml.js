const path = require('path')
const fs = require('fs')
const pug = require('pug')
fs.readdirSync('.').forEach(f => {
    console.log(f)
    if(path.parse(f).ext == '.pug') {
        console.log("match")
        var compiled = (pug.compile(fs.readFileSync(f))).call(this)
        fs.writeFileSync(`${path.parse(f).name}.html`, compiled)
    }
})
