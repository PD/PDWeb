import jester, nwt, asyncdispatch

var templates = newNwt("public/*.html")

router pdweb:
    get "/": resp templates.renderTemplate("index.html")
    get "/contact": resp templates.renderTemplate("contact.html")
    get "/about": resp templates.renderTemplate("about.html")
    get "/apply": resp templates.renderTemplate("apply.html")

proc main() =
    let port = 8080.Port
    let settings = newSettings(port=port)
    var jester_server = initJester(pdweb, settings=settings)
    jester_server.serve()

when isMainModule: # only runs if it's ran directly instead of imported
    main()
